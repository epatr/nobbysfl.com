# nobbysfl.com

Website for Nobby's Bar & Cafe in St. Augustine, FL. Currently in development.

## Versions

### 0.0.2 (September 2015)

Removed all JavaScript references and all unused LESS files from the project.
`main.css` went from 140kb to 25. JavaScript went from 150kb to 15. `index.html`
went from 1.9kb to 1.5.


### 0.0.1 (March 2015)

Started a fresh project with a full Bootstrap install using LESS.